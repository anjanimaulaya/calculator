package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import net.objecthunter.exp4j.ExpressionBuilder

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Numbers
        bu1.setOnClickListener { appendOnExpression("1", true) }
        bu2.setOnClickListener { appendOnExpression("2", true) }
        bu3.setOnClickListener { appendOnExpression("3", true) }
        bu4.setOnClickListener { appendOnExpression("4", true) }
        bu5.setOnClickListener { appendOnExpression("5", true) }
        bu6.setOnClickListener { appendOnExpression("6", true) }
        bu7.setOnClickListener { appendOnExpression("7", true) }
        bu8.setOnClickListener { appendOnExpression("8", true) }
        bu9.setOnClickListener { appendOnExpression("9", true) }
        bu0.setOnClickListener { appendOnExpression("0", true) }
        buDot.setOnClickListener { appendOnExpression(".", true) }
        buPercent.setOnClickListener{appendOnExpression("%", true)}
        buPlusMin.setOnClickListener {appendOnExpression("-", true)}

        //Operators
        buAdd.setOnClickListener { appendOnExpression("+", false) }
        buSub.setOnClickListener { appendOnExpression("-", false) }
        buMul.setOnClickListener { appendOnExpression("*", false) }
        buDiv.setOnClickListener { appendOnExpression("/", false) }

        buDel.setOnClickListener {
            showData.text = ""
            showResult.text = ""
        }

        buPercent.setOnClickListener{
            val number = showData.text.toString().toDouble()/100
            showData.text = number.toString()
        }

        buEqual.setOnClickListener {
            try {

                val expression = ExpressionBuilder(showData.text.toString()).build()
                val result = expression.evaluate()
                    showResult.text = result.toString()

            }catch (e:Exception){
                Log.d("Exception"," message : " + e.message )
            }
        }


    }

    fun appendOnExpression(string: String, canClear: Boolean) {

        if(showResult.text.isNotEmpty()){
            showData.text = ""
        }
        if (canClear) {
            showResult.text = ""
            showData.append(string)
        } else {
            showData.append(showResult.text)
            showData.append(string)
            showResult.text = ""
        }
    }


}

